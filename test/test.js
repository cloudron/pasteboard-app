#!/usr/bin/env node

'use strict';

var child_process = require('child_process'),
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    uuid = require('uuid'),
    url = require('url');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);

    var LOCATION = 'pasteboardtest';
    var hostname, appId, appUrl, imageId, finalImageUrl;
    var TEST_IMAGE_PATH = path.resolve('test.jpg');
    var TEMP_IMAGE_PATH = '/tmp/test.jpg';

    xit('build app', function () {
        child_process.execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        child_process.execSync('cloudron install --new --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get the main page', function (done) {
        var inspect = JSON.parse(child_process.execSync('cloudron inspect'));
        hostname = inspect.fqdn;
        appId = inspect.apps.find(function (e) { return e.location === LOCATION; }).id;
        appUrl = 'https://' + LOCATION + '.' + hostname;

        superagent.get(appUrl).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can upload image', function (done) {
        imageId = uuid.v4();

        superagent
          .post(appUrl + '/upload')
          .field('id', imageId)
          .attach('file', path.resolve('test.jpg'))
          .end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);
            expect(result.body.url).to.be.a('string');

            finalImageUrl = result.body.url;

            done();
        });
    });

    it('can get image page', function (done) {
        superagent.get(finalImageUrl)
          .end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can get image', function (done) {
        var rawImageUrl = appUrl + '/storage' + url.parse(finalImageUrl).path;
        var stream = fs.createWriteStream(TEMP_IMAGE_PATH);

        var req = superagent.get(rawImageUrl);
        req.pipe(stream);
        req.on('error', done);
        req.on('end', function () {
            expect(fs.statSync(TEMP_IMAGE_PATH).size).to.eql(fs.statSync(TEST_IMAGE_PATH).size);
            done();
        });
    });

    it('backup app', function () {
        child_process.execSync('cloudron backup create --app ' + appId, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        child_process.execSync('cloudron restore --app ' + appId, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get the main page after restore', function (done) {
        superagent.get(appUrl).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can get image page', function (done) {
        superagent.get(finalImageUrl)
          .end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can get image', function (done) {
        var rawImageUrl = appUrl + '/storage' + url.parse(finalImageUrl).path;
        var stream = fs.createWriteStream(TEMP_IMAGE_PATH);

        var req = superagent.get(rawImageUrl);
        req.pipe(stream);
        req.on('error', done);
        req.on('end', function () {
            expect(fs.statSync(TEMP_IMAGE_PATH).size).to.eql(fs.statSync(TEST_IMAGE_PATH).size);
            done();
        });
    });

    it('can restart app', function (done) {
        child_process.execSync('cloudron restart', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        done();
    });

    it('can get image page', function (done) {
        superagent.get(finalImageUrl)
          .end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can get image', function (done) {
        var rawImageUrl = appUrl + '/storage' + url.parse(finalImageUrl).path;
        var stream = fs.createWriteStream(TEMP_IMAGE_PATH);

        var req = superagent.get(rawImageUrl);
        req.pipe(stream);
        req.on('error', done);
        req.on('end', function () {
            expect(fs.statSync(TEMP_IMAGE_PATH).size).to.eql(fs.statSync(TEST_IMAGE_PATH).size);
            done();
        });
    });

    it('uninstall app', function () {
        child_process.execSync('cloudron uninstall --app ' + appId, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
