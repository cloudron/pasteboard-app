# Pasteboard Cloudron App

This repository contains the Cloudron app package source for [Pasteboard](https://github.com/JoelBesada/pasteboard).

## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=co.pasteboard.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id co.pasteboard.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd pasteboard-app
cloudron build
cloudron install
```


