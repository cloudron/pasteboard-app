Pasteboard is a quick and simple image sharing web app driven and developed by Joel Besada.

This app packages Pasteboard <upstream>9f25033c04</upstream>.

Images uploaded to Pasteboard are publicly available but not broadcasted, meaning it is up
to you to decide who you share the images with.

Pasteboard supports uploading via drag & drop, copy & paste or taking pictures with a webcam.

* Chrome extension: <https://github.com/JoelBesada/pasteboard-extension>

