#!/bin/bash

set -eu

export ORIGIN="${APP_ORIGIN}"
export NODE_ENV=production

mkdir -p /run/builtAssets

chown cloudron:cloudron -R /app/data
chown cloudron:cloudron -R /run

cd /app/code

exec /usr/local/bin/gosu cloudron:cloudron coffee app.coffee
