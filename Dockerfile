FROM cloudron/base:0.10.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io>

ENV PATH /usr/local/node-0.10.48/bin:$PATH

RUN mkdir -p /app/code
WORKDIR /app/code

# use our dev branch (https://github.com/JoelBesada/pasteboard/pull/45)
RUN curl -L https://github.com/cloudron-io/pasteboard/tarball/4f83c74e03edb1c98a76b8ee51a82caca992d753 | tar -xz --strip-components 1 -f -
RUN npm install --production
RUN npm -g install coffee-script forever

RUN rm -rf /app/code/public/storage && ln -s /app/data /app/code/public/storage && \
    rm -rf /app/code/builtAssets && ln -s /run/builtAssets /app/code/builtAssets

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
